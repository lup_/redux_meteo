const path = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require("webpack");

module.exports = {
  entry: {
      //app: "./src/index.js",
      css: "./src/styles/main.scss"
  },
  output: {
    filename: "bundle.css",
    path: path.resolve(__dirname, "./build/static/css/"),
    publicPath: '/build/static/css/',
  },
  module: {
    rules: [
      {
        test: /\.scss/,
        include: path.resolve(__dirname,	"src")+'/styles/main.scss',
        loader: ExtractTextPlugin.extract(["css-loader", "sass-loader"])
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      },
      {
        test: /\.svg$/,
        loader: 'svg-loader'
      },
      {
        test: /\.js$/,
        include: [
            path.resolve(__dirname,	"src"),
        ],
        loaders: "eslint-loader",
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: "babel-loader",
        options: {
          presets: ["react", "stage-0", "es2015"],
          plugins: ["transform-class-properties", "transform-decorators-legacy"]
        }
      }
    ]
  },
  devServer: {
    contentBase: "%PUBLIC_URL%/"
  },
  plugins: [
    new ExtractTextPlugin("bundle.css"),
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify("production")
    }),
    new webpack.optimize.UglifyJsPlugin()
  ],


};