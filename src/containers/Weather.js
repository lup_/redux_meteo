import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Forecast from './Forecast'
import CurrentWeather from '../components/CurrentWeather'
import '../styles/Weather.css'
import axios from 'axios'
import * as apiConst from '../constants/API'
import * as weatherActions from '../actions/WeatherActions'


export default class Weather extends Component {

    render() {
        const { city, temperature, cloud, wind, icon, fetching } = this.props.CurrentWeather
        const forecast  = this.props.Forecast

        const { getWeather, getForecast } = this.props.weatherActions

        return <div className='weather'>

            <CurrentWeather
                city={city}
                getWeather={getWeather}
                fetching={fetching}
                temperature={temperature}
                wind={wind}
                icon={icon}
                cloud={cloud}
            />

            <Forecast
                city={city}
                dayForecasts={forecast}
                getForecast={getForecast}
            />
        </div>
    }
}


Weather.propTypes = {
    //forecast: PropTypes.instanceOf(Forecast),
    //CurrentWeather: PropTypes.instanceOf(CurrentWeather),
    //weatherActions: PropTypes.instanceOf(weatherActions),
    Forecast: PropTypes.instanceOf(Forecast)
}