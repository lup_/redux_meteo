import React, { Component } from 'react'
import { connect } from 'react-redux'
import logo from '../logo.svg'
import Weather from './Weather'
import Favorites from '../components/Favorites'
import '../styles/App.css'
import bindActionCreators from 'redux/src/bindActionCreators'
import * as weatherActions from '../actions/WeatherActions'

class App extends Component {
    render() {
        const { weather, forecast, favorites } = this.props
        const weatherActions = this.props.weatherActions

        return (
            <div className='App'>
                <header className='App-header'>
                    <img src={logo} className='App-logo' alt='logo' />
                    <h1 className='App-title'>Welcome to React</h1>
                </header>
                <div className='meteo-app'>
                    <Weather
                        CurrentWeather={weather}
                        weatherActions={weatherActions}
                        Forecast={forecast}
                    />
                    <Favorites />
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        weather: state.weather,
        favorites: state.favorites,
        forecast: state.forecast
    }
}

function mapDispatchToProps(dispatch) {
    return {
        weatherActions: bindActionCreators(weatherActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
//export default App;
