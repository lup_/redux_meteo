import React, { Component } from 'react'
import PropTypes from 'prop-types'
import '../styles/Forecast.css'
import DayForecast from '../components/DayForecast'
import * as weatherActions from '../actions/WeatherActions'

export default class Forecast extends Component {

    componentDidMount() {
        this.props.getForecast(this.props.city)
    }

    render(){

        let days = this.props.dayForecasts
        let dayTemplate

        console.log('!!!')
        console.log(days)
        console.log(this.props.dayForecasts)

        if (! days == null){
            dayTemplate = days.map(function(item, index) {
                return (
                    <div key={index}>
                        <DayForecast
                            temperatureDay={25}
                            temperatureNight={14}
                            wind={'sw'}
                            weekday={'Sat'}
                            icon={''}
                        />
                    </div>
                )
            })
        }
        else{
            dayTemplate=<p>Прогноза не будет</p>
        }

        return (
            <div className='forecasts'>
                {dayTemplate}
            </div>
        )
    }

}


Forecast.propTypes = {
    // dayForecasts: PropTypes.array().isRequired(),
    getForecast: PropTypes.func.isRequired
}