import axios from 'axios'
import * as weatherConsts from '../constants/Weather'
import * as apiConst from '../constants/API'

export function getWeather(city){

    return (dispatch) => {

        dispatch({
            type: weatherConsts.GET_WEATHER_REQUEST,
            payload: 'Loading...'
        })

        axios.get(apiConst.API_URL+'/weather',{
            params: {
                appid: apiConst.API_KEY,
                q: city
            }
        })
            .then(res => {
                let temperature = Math.floor(res.data.main.temp - 273)
                return temperature
            })
            .then((temperature) => {
                dispatch({
                    type: weatherConsts.GET_WEATHER_SUCCESS,
                    payload: temperature
                })
            })
    }

}

export function getForecast(city){

    return (dispatch) => {

        dispatch({
            type: weatherConsts.GET_FORECAST_REQUEST,
            payload: 'Loading...'
        })

        axios.get(apiConst.API_URL+'/forecast',{
            params: {
                appid: apiConst.API_KEY,
                q: city
            }
        })
            .then(res => {
                dispatch({
                    type: weatherConsts.GET_FORECAST_SUCCESS,
                    payload: res
                })
            })
            .then(res => {
                dispatch({
                    type: weatherConsts.SET_DAILY_FORECAST,
                    payload: res
                })
            })
    }

}
