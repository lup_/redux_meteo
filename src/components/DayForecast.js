import React, { Component } from 'react'
import PropTypes from 'prop-types'
import '../styles/Weather.css'

export default class DayForecast extends Component {


    render() {
        const { temperatureDay, temperatureNight, wind, weekday, icon } = this.props

        return <div className='weather'>
            <div class='weather-forecast__daily-forecast'>
                <p class='weather-forecast__weekday weather-forecast__weekday_today'>{weekday}</p>
                <img class='weather-forecast__cloudiness-icon' src={icon}/>
                <div class='weather-forecast__temperatures'>
                    <p class='weather-forecast__temperature temperature_day'>
                        <span class='weather-forecast__celsius'>{temperatureDay}<span class='weather-forecast__degrees'>o</span>C</span>
                    </p>
                    <p class='weather-forecast__temperature temperature_night'>
                        <span class='weather-forecast__celsius'>{temperatureNight}<span class='weather-forecast__degrees'>o</span>C</span>
                    </p>
                </div>
                <p class='weather-forecast__windiness'>{wind}</p>
            </div>
        </div>
    }
}


DayForecast.propTypes = {
    temperatureDay: PropTypes.string.isRequired,
    temperatureNight: PropTypes.string.isRequired,
    wind: PropTypes.string.isRequired,
    weekday: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired
}