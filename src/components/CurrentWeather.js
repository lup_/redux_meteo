import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Forecast from '../containers/Forecast'
import '../styles/Weather.css'
import axios from 'axios'
import * as apiConst from '../constants/API'

export default class CurrentWeather extends Component {

    onYearBtnClick(e) {
        this.props.getWeather(this.props.city)
    }

    componentDidMount() {
        this.props.getWeather(this.props.city)
    }

    render() {
        const { city, temperature, cloud, wind, icon, fetching} = this.props

        return <div className='weather'>
            <div className={'fetching_'+fetching}>
                Loading . . .
            </div>

            <p className='weather__title'>{city}</p>
            <div className='weather__current-state'>
                <div className='weather__current-weather'>
                    <div className='weather__cloud-wind'>
                        <p className='weather__cloudiness'>{cloud}</p>
                    </div>
                    <div className='weather__cloudiness-temp'>
                        <img src={icon} className='weather__cloudiness-icon'/>
                        <p className='weather__temperature'>
                            <span className='weather__celsius'>{temperature}
                                <span className='weather__degrees'>o</span>C
                            </span>
                        </p>
                    </div>
                </div>
                <div className='weather__current-junk-data'>
                    <p className='weather__windiness'>{wind}</p>
                </div>
            </div>
            <button onClick={this.onYearBtnClick.bind(this)}>
                получить погоду
            </button>

        </div>
    }
}


CurrentWeather.propTypes = {
    city: PropTypes.string.isRequired,
    getWeather: PropTypes.func.isRequired,
    fetching: PropTypes.bool.isRequired,
    temperature: PropTypes.number.isRequired,
    wind: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    cloud: PropTypes.string.isRequired
}