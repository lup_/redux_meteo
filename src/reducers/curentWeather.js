import * as weatherConsts from '../constants/Weather'

const initialState = {
    city: 'Tomsk',
    temperature: 0,
    cloud: '',
    wind: '',
    icon: '',
    fetching: false
}

export default function weather(state = initialState, action){

    switch(action.type){

        case weatherConsts.GET_WEATHER_REQUEST:
            return {
                ...state,
                temperature: action.payload,
                fetching: true
            }

        case weatherConsts.GET_WEATHER_SUCCESS:
            return {
                ...state,
                temperature: action.payload,
                fetching: false
            }

        default:
            return state
    }


}