import * as weatherConsts from '../constants/Weather'

const initialState = {
    city: 2016,
    temperature: 0,
    cloud: '',
    wind: '',
    icon: '',
    fetching: false
};

export default function weather(state = initialState, action){

    switch(action.type){

        case weatherConsts.GET_WEATHER_REQUEST:
            return {
                ...state,
                city: action.payload,
                fetching: true
            };

        case weatherConsts.GET_WEATHER_SUCCESS:
            return {
                ...state,
                city: action.payload,
                fetching: false
            };

        default:
            return state;
    }


}