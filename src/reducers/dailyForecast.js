import * as weatherConsts from '../constants/Weather'

const initialState = {
    temperatureDay: -273,
    temperatureNight: -273,
    wind: '',
    weekday: '',
    icon: ''
}


//Доделать обработку приема результата, распарсить и записхуть в стейт
export default function dailyForecast(state = initialState, action){

    switch(action.type){

        case weatherConsts.SET_DAILY_FORECAST:
            return {
                ...state,
                temperatureDay: 25,
                temperatureNight: 15,
                wind: 'sw',
                weekday: 'Sat',
                icon: ''
            }

        default:
            return state
    }


}