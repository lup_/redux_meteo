import { combineReducers } from 'redux'
import weather from './curentWeather'
import forecast from './forecast'
import dailyForecast from './dailyForecast'
import favorites from './favorites'

export default combineReducers({
    weather,
    forecast,
    dailyForecast,
    favorites
})