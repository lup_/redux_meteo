import * as weatherConsts from '../constants/Weather'

const initialState = {
    city:'Tomsk',
    dayForecasts: []

}


//Доделать обработку приема результата, распарсить и записхуть в стейт
export default function forecast(state = initialState, action){

    switch(action.type){

        case weatherConsts.GET_FORECAST_REQUEST:
            return {
                ...state
            }

        case weatherConsts.GET_FORECAST_SUCCESS:
            return {
                ...state,
                dayForecasts: action.payload.data.list
            }

        default:
            return state
    }


}